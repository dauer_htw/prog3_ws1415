#ifndef MYMATH_H
#define MYMATH_H



/*
 * returns the the value of factorial n
 * 
 * */
int fac(int n);



/*
 * returns the the value of the position n in the fibonacci sequence 
 * 
 * */
int fib(int n);

/*
 * prints n numbers of the fibonacci sequence
 * 
 * */
void printFib(int n);


#endif