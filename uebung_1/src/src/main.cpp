#include <iostream>
#include "../include/mymath.h"
using namespace std;


// Define new namespace
namespace bettermath{
	
	int fac(int n){
		int result = 1;
		if(n != 0){
			for (int i=1; i<=n; i++){
				result *= i;
			}
		}
		return result;
	}
}

int main(int argc, char **argv)
{
	
	    
	cout << fac(5) << endl;	
	cout << bettermath::fac(3) << endl;	
	
	printFib(10);

	int a[5] = {1,2,3,4,5};
	
	// for each loop in c++11 
	for(int zahl : a){
		cout << zahl << endl ;
	}
	
}
