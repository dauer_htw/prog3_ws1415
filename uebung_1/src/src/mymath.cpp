#include "../include/mymath.h"
#include <iostream>

using namespace std;

int fib(int n){
	return n <= 2 ? 1 : n = fib(n-2) + fib(n-1);
}

void printFib(int n){
	for (int i=1; i <= n; i++){
		cout << "n: " << i << "\tfib(n): " << fib(i) << endl;
	}
}

int fac(int n){
	return n == 0 ? 1 : n * fac(n-1);
}
