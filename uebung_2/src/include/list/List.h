#ifndef LIST_H
#define LIST_H
#include "Node.h"
class List
{
	protected:
		Node* _first;
		Node* _last;
		int _size;
	public:
		List() : _first(0), _last(0), _size(0) {};
		virtual ~List();
		virtual void* add(void* data) = 0;
		virtual bool remove(int index) = 0;
		virtual void* get(int index) = 0;
		virtual int getSize() = 0;

};

#endif // LIST_H
