#ifndef NODE_H
#define NODE_H

class Node {
	private:
		Node* _next;
		Node* _prev;
		void* _data;
	public:
		Node(void* data, Node* prev, Node* next);
		~Node();
		void setNext(Node* next);
		void setPrev(Node* prev);
		void* getData();
		Node* getNext();
		Node* getPrev();

};

#endif // NODE_H
