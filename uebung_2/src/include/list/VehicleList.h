#ifndef VEHICLELIST_H
#define VEHICLELIST_H
#include "List.h"

class VehicleList : public List
{
	
	private:
		void deleteData(Node* nodeToDelete);
		Node* find(int index);
		
	public:
		VehicleList() {};
		virtual ~VehicleList();
		void* add(void* data);
		bool remove(int index);
		void* get(int index);
		void drop();
		int getSize();

};

#endif // VEHICLELIST_H
