#ifndef BOAT_H
#define BOAT_H
#include "Vehicle.h"

class Boat : public virtual Vehicle
{
public:
	Boat(string name, double speed) : Vehicle(name, speed) {};
	~Boat();
	void move();
};

#endif // BOAT_H
