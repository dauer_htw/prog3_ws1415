#ifndef CAR_H
#define CAR_H
#include "Vehicle.h"

class Car : public virtual Vehicle
{
public:
	Car(string name, double speed) : Vehicle(name, speed) {};
	~Car();
	virtual void move();
	
};

#endif // CAR_H
