#ifndef ANIMAL_H
#define ANIMAL_H
#include <string>

using namespace std;

class Vehicle
{
	private:
		static int number;
		string _name;
		int _id;
		double _speed;
	public:
		Vehicle(string name, double speed);
		virtual ~Vehicle();
		virtual string getName();
		virtual int getId();
		virtual double getSpeed();
		virtual void move();
		

};

#endif // ANIMAL_H
