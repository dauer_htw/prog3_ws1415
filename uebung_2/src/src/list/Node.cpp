#include "../../include/list/Node.h"

Node::Node(void* data, Node* prev, Node* next){
		_data = data;
		_prev = prev;
		_next = next;
}	
Node::~Node()
{
}


void Node::setNext(Node* next){ _next = next; };
void Node::setPrev(Node* prev){ _prev = prev; };
void* Node::getData(){ return _data;};
Node* Node::getNext(){ return _next;};
Node* Node::getPrev(){ return _prev; };
