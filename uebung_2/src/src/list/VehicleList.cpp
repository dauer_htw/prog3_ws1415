#include "../../include/list/VehicleList.h"
#include "../../include/vehicle/Vehicle.h"

VehicleList::~VehicleList()
{
	this->drop();
}

void VehicleList::deleteData(Node* nodeToDelete){
	if (nodeToDelete != 0){
		if(nodeToDelete == _first){ _first = nodeToDelete->getNext(); }
		Vehicle* dataToDelete = (Vehicle*) nodeToDelete->getData();
		delete dataToDelete;
		delete nodeToDelete;
		_size--;
	}
}
void* VehicleList::add(void* data) {
	
	if(_first == 0){
		_first = new Node(data, 0, 0);
		_last = _first;
	}else{
		Node* newNode = new Node(data, _last, 0);
		_last->setNext(newNode);
		_last = newNode;
	}

	_size++;
	return _last->getData();
	
};

Node* VehicleList::find(int index){
	Node* node = 0;
	
	if(index < 0) { return node; } //Error! Negative index
	if(index == 0) { return _first; }
	if(index == _size-1) { return _last; }
	
	if(index <= _size / 2 ){
		node = _first;
		for (int i = 0; i < index; i++){
			node = node->getNext();
		}
	}else{
		node = _last;
		for (int i = _size-1; i > index; i--){
			node = node->getPrev();
		}
	}
	
	return node;
}
bool VehicleList::remove(int index) {
	bool result = false;
	Node* nodeToDelete = this->find(index);
	if(nodeToDelete != 0){
		this->deleteData(nodeToDelete);
		result = true;
	}
	return result;
};

void* VehicleList::get(int index) {
	Vehicle* data = 0;
	if(index <= _size){
		Node* dataNode = this->find(index);
		if(dataNode != 0){
			data = (Vehicle*) dataNode->getData();
		}
	}	
	return data;
};

void VehicleList::drop(){
	Node* nextNode = _first;
	while(_first != 0){
		nextNode = _first->getNext();
		this->deleteData(_first);
		_first = nextNode;
	}
};

int VehicleList::getSize() { return _size; }