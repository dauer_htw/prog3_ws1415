#include <iostream>
#include "../include/vehicle/Car.h"
#include "../include/vehicle/Boat.h"
#include "../include/list/VehicleList.h"

using namespace std;

int main(){

	List* vehicleList = new VehicleList;
	Vehicle* car = 0;
	Vehicle* boat = 0;
	Vehicle* car2 = 0;
	Vehicle* boat2 = 0;
	Vehicle* boat3 = 0;
	
	car = new Car("newCar", 10);
	boat = new Boat("newBoat", 1);
	car2 = new Car("car2", 2);
	boat2 = new Boat("newBoat2", 100);
	boat3 = new Boat("newBoat3", 50);
	
	vehicleList->add(car);
	vehicleList->add(boat);
	vehicleList->add(car2);
	vehicleList->add(boat2);
	vehicleList->add(boat3);
	
	for(int i=0; i < vehicleList->getSize(); i++){
		Vehicle* v = ((Vehicle* )vehicleList->get(i));
		cout << "id: " << v->getId() << " move : ";
		v->move();
	}

	delete vehicleList;
	
}