#include "../../include/vehicle/Vehicle.h"
#include <iostream>

using namespace std;

int Vehicle::number = 1;
Vehicle::Vehicle(string name, double speed) {
	_name = name;
	_speed = speed;
	_id = number;
	number++;
}
Vehicle::~Vehicle()
{
	number--;
}

void Vehicle::move(){
	cout << "vehicle moves... somehow" << endl;
}

int Vehicle::getId(){return _id;}
string Vehicle::getName() { return _name; };
double Vehicle::getSpeed() {return _speed;};
