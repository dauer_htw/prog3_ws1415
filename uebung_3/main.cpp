#include <stdio.h>
#include "src/src/Sorter.h"
#include "src/src/SelectionSort.h"
#include "src/src/Triangle.h"
#include <iostream>

using namespace std;

void printAll(Sortable** values, int len){
	for(int i = 0; i < len; i++){
		((Triangle*)values[i])->print();
	}
}
Sortable** createTriangleList(){
	Triangle* triangle_1 = new Triangle(1,2,3);	
	Triangle* triangle_2 = new Triangle(4,5,6);
	Triangle* triangle_3 = new Triangle(7,8,9);
	Triangle* triangle_4 = new Triangle(10,11,12);
	Triangle* triangle_5  =new Triangle(13,14,15);



	return new Sortable*[5] {	triangle_4, triangle_5, 
								triangle_2, triangle_1, 
								triangle_3 };
	
}

void clearList(Sortable** list, int len){
	for( int i = 0; i < len; i++){
		delete (Triangle*)list[i];
	}
}

int main(int argc, char **argv)
{
	
//	Triangle triangle_1(1,2,3);	
//	Triangle triangle_2(4,5,6);
//	Triangle triangle_3(7,8,9);
//	Triangle triangle_4(10,11,12);
//	Triangle triangle_5(13,14,15);
//	
	
//	Sortable* triangleList[] = {&triangle_4, &triangle_5, &triangle_2, 
//						&triangle_1, &triangle_3};



	Sortable** list = createTriangleList();
	Sorter* sorter = new SelectionSort();
	
	cout << "Before sort: " << endl;
	printAll(list, 5);
	
	sorter->sort(list, 5);
	cout << endl << endl;
	cout << "After sort: " << endl;
	cout << endl << endl;
	printAll(list, 5);
	
	
	clearList(list, 5);
	delete[] list;
	
	delete (SelectionSort*)sorter;
	
	return 0;	
}
