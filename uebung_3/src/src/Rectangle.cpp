#include "Rectangle.h"
#include <typeinfo>

Rectangle::~Rectangle()
{
}

bool Rectangle::gt(Sortable* a) { 
	
	
//	if(dynamic_cast<Rectangle*>(a) != 0){
//		Rectangle* other = (Rectangle*) a;
//		return _a > other->getA() && _b > other->getB();
//	}

	if(typeid(*a) == typeid(Rectangle)){
		Rectangle* other = (Rectangle*) a;
		return _a > other->getA() && _b > other->getB();
	}
	return false;
}

double Rectangle::getA() { return _a; };
double Rectangle::getB() { return _b; };
double Rectangle::getArea() { return _a * _b;} ;
	