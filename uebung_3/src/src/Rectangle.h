#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "Sortable.h"

class Rectangle : public Sortable
{
	private:
		double _a, _b;
	public:
		Rectangle(double a, double b) : _a(a), _b(b) {};
		~Rectangle();
		virtual bool gt(Sortable* a);
		double getA();
		double getB();
		double getArea();
	
};

#endif // RECTANGLE_H
