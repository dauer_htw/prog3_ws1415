#include "SelectionSort.h"

SelectionSort::SelectionSort()
{
}

SelectionSort::~SelectionSort()
{
}

void swap(Sortable** values, int a, int b){
	Sortable* tmp = values[a]; 
	values[a] = values[b];
	values[b] = tmp;
}

/* Implementation: https://en.wikipedia.org/wiki/Selection_sort */ 
void SelectionSort::sort(Sortable** values, int len) {
	int iMin = 0;
	for (int i = 0; i < len-1; i++) {
		iMin = i;
		for ( int j = i+1; j < len; j++) {
			if (!values[j]->gt(values[iMin])) {
				iMin = j;
			}
		}
	 
		if(iMin != i) {
			swap(values, i, iMin);
		}
	 
	}
}