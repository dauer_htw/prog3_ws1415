#ifndef SELECTIONSORT_H
#define SELECTIONSORT_H
#include "Sorter.h"

class SelectionSort : public Sorter
{
public:
	SelectionSort();
	virtual ~SelectionSort();
	void sort(Sortable** values, int len);
};

#endif // SELECTIONSORT_H
