#ifndef SORTABLE_H
#define SORTABLE_H

class Sortable
{
	public:
		virtual bool gt(Sortable* a) = 0; 
};

#endif // SORTABLE_H
