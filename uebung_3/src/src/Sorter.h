#ifndef SORTER_H
#define SORTER_H
#include "Sortable.h"

class Sorter
{
	public:
		virtual void sort(Sortable** values, int len) = 0;
};

#endif // SORTER_H
