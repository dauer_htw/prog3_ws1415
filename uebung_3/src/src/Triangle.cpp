#include "Triangle.h"
#include <iostream>
#include <cmath>
#include <typeinfo>

using namespace std;

Triangle::Triangle(double a, double b, double c) : _a(a), _b(b), _c(c) {
	_u = (_a+_b+_c);
}

Triangle::~Triangle(){}

void Triangle::print(){
	cout << "----------" << endl;
	cout << "a: " << _a << endl;
	cout << "b: " << _b << endl;
	cout << "c: " << _c << endl;
	cout << "u: " << _u << endl;
	cout << "area: " << this->getArea() << endl;
	cout << "----------" << endl;
}

double Triangle::getArea(){
	double halfU = _u / 2;
	double tmp = (halfU * (halfU - _a) * (halfU - _b) * (halfU - _c));
	return sqrt(tmp);
}

bool Triangle::gt(Sortable* sortable){

	if (typeid(*sortable) == typeid(Triangle)){
			Triangle* otherTriangle = (Triangle*) sortable;
			return getArea() > otherTriangle->getArea();
	}
	
	return false; 
}

double Triangle::getA() { return _a; };
double Triangle::getB() { return _b; };
double Triangle::getC() { return _c; };
double Triangle::getU() { return _u; };