#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "Sortable.h"

class Triangle : public Sortable
{
	private:
		double _a, _b, _c;
		double _u;
	public:
		Triangle(double a, double b, double c);
		virtual ~Triangle();
		virtual bool gt(Sortable* a); 
		void print();
		double getArea();
		double getA();
		double getB();
		double getC();
		double getU();
};

#endif // TRIANGLE_H
