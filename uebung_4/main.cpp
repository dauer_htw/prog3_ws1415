#include <stdio.h>
#include <iostream>
#include "src/src/Sorter.h"
#include "src/src/Triangle.h"
#include "src/src/Rectangle.h"
#include "src/src/SelectionSort.h"
#include "src/src/SelectionSort.cpp"

using namespace std;

template <class T>
void printAll(T** values, int len){
	for(int i = 0; i < len; i++){
		(values[i])->print();
	}
}

template <class T>
T** createList(){
	T* triangle_1 = new T();	
	T* triangle_2 = new T();
	T* triangle_3 = new T();
	T* triangle_4 = new T();
	T* triangle_5  =new T();

	return new T*[5] {	triangle_4, triangle_5, 
								triangle_2, triangle_1, 
								triangle_3 };
	
}

Triangle** createTriangleList(){
	Triangle* triangle_1 = new Triangle(1,2,3);	
	Triangle* triangle_2 = new Triangle(4,5,6);
	Triangle* triangle_3 = new Triangle(7,8,9);
	Triangle* triangle_4 = new Triangle(10,11,12);
	Triangle* triangle_5  =new Triangle(13,14,15);



	return new Triangle*[5] {	triangle_4, triangle_5, 
								triangle_2, triangle_1, 
								triangle_3 };
	
}


Rectangle** createRectangleList(){
	Rectangle* triangle_1 = new Rectangle(1,2);	
	Rectangle* triangle_2 = new Rectangle(4,5);
	Rectangle* triangle_3 = new Rectangle(7,8);
	Rectangle* triangle_4 = new Rectangle(10,11);
	Rectangle* triangle_5  =new Rectangle(13,14);



	return new Rectangle*[5] {	triangle_4, triangle_5, 
								triangle_2, triangle_1, 
								triangle_3 };
	
}



template <class T>
void clearList(T** list, int len){
	for( int i = 0; i < len; i++){
		delete list[i];
	}
}

class Foo : Sortable<Foo>{
	public:
		virtual bool gt(Foo* other){ return false; }
};

int main(int argc, char **argv)
{
	Triangle** list = createTriangleList();
	Sorter<Triangle>* sorter = new SelectionSort<Triangle>();
	
	cout << "Before sort: " << endl;
	printAll(list, 5);
	
	sorter->sort(list, 5);
	
	cout << endl << endl;
	cout << "After sort: " << endl;
	cout << endl << endl;
	printAll(list, 5);
	
	
	clearList(list, 5);
	delete[] list;
	
	delete sorter;
	
	return 0;	
}
