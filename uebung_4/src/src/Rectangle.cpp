#include "Rectangle.h"
#include <typeinfo>
#include <iostream>


using namespace std;

Rectangle::~Rectangle()
{
}

bool Rectangle::gt(Rectangle* b) { 
	return _a > b->getA() && _b > b->getB();
}

void Rectangle::print(){
	cout << endl;
	cout << "a : " << _a << endl;
	cout << "b : " << _b << endl;
	cout << "area : " << this->getArea() << endl;
};

double Rectangle::getA() { return _a; };
double Rectangle::getB() { return _b; };
double Rectangle::getArea() { return _a * _b;} ;
	