#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "Sortable.h"

class Rectangle : public Sortable<Rectangle>
{
	private:
		double _a, _b;
	public:
		Rectangle() : _a(0), _b(0) {};
		Rectangle(double a, double b) : _a(a), _b(b) {};
		~Rectangle();
		virtual bool gt(Rectangle* a);
		void print();
		double getA();
		double getB();
		double getArea();
	
};

#endif // RECTANGLE_H
