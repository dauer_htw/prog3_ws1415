#ifndef SELECTION_SORT_H
#define SELECTION_SORT_H
#include "Sorter.h"
#include "Sortable.h"

template<typename T>

class SelectionSort : public Sorter<T> 
{
	private:
		void swap(T** values, int a, int b);
	public:
		SelectionSort(){};
		virtual ~SelectionSort(){};
		void sort(T** values, int len);
};

#endif