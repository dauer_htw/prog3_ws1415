#ifndef SORTABLE_H
#define SORTABLE_H

template <class T>
class Sortable
{
	public:
		virtual ~Sortable(){};
		virtual bool gt(T* a) = 0;
};

#endif // SORTABLE_H
