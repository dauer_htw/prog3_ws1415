#ifndef SORTER_H
#define SORTER_H
#include "Sortable.h"

template <class T>
class Sorter
{
	public:
		Sorter() {
			/* C++11 static_assert */
			static_assert(std::is_base_of<Sortable<T>, T>::value, 
			"type parameter of this class must derive from Sortable<T>");
		};
		virtual ~Sorter(){};
		virtual void sort(T** values, int len) = 0;
};

#endif // SORTER_H
