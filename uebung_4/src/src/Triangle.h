#include "Sortable.h"
#ifndef TRIANGLE_H
#define TRIANGLE_H

class Triangle : public Sortable<Triangle>
{
	private:
		double _a, _b, _c;
		double _u;
	public:
		Triangle() : _a(0), _b(0), _c(0), _u(0){};
		Triangle(double a, double b, double c);
		virtual ~Triangle();
		virtual bool gt(Triangle* a); 
		virtual Triangle* get(); 
		void print();
		double getArea();
		double getA();
		double getB();
		double getC();
		double getU();
};
#endif