#include <iostream>
#include "src/include/Complex.h"
#include "src/include/Stack.h"
#include "src/include/ArrayStack.h"
#include "src/include/InvalidArgumentArrayStackException.h"
#include "src/include/OutOfRangeArrayStackException.h"

using namespace std;

int main(int argc, char **argv)
{

	try{
		Stack<Complex>* cStack = new ArrayStack<Complex>(5);
		
		Complex c1(42.0, 23.0);
		Complex c2(2.0, 5.0);
		Complex c3(7.0, 27.0);
		Complex c4(c3);
		Complex c5(1,1);
		
		cStack->push(c1);
		cStack->push(c2);
		cStack->push(c3);
		cStack->push(c4);
		cStack->push(c5 + c3);
		
		for (int i = 0; i<5; i++){	
			cout << cStack->pop() << endl;
		}
		
	}catch(ArrayStackException e){
		cout << e.what() << endl;
		return 1;
	}
		
	return 0;
}
