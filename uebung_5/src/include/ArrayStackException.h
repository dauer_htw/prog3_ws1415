#ifndef ARRAYSTACKEXCEPTION_H
#define ARRAYSTACKEXCEPTION_H
#include <stdexcept>
#include <string>

class ArrayStackException : public std::runtime_error
{
	private:
		static std::string createErrMsg(const std::string& msg);
	public:
		ArrayStackException(const std::string& msg);
		virtual ~ArrayStackException();

};

#endif // ARRAYSTACKEXCEPTION_H
