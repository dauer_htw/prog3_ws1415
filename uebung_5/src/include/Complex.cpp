#include "Complex.h"
#include <cmath>
#include <sstream>

using namespace std;

Complex::Complex() : Complex(0,0){}
Complex::Complex(double rel) : Complex(rel, 0){}
Complex::Complex(double rel, double img){
	_rel = rel;
	_img = img;
}

string Complex::toString() const{
//	(1,2i)
	ostringstream format(ios::out);
	format << "(" << _rel << "," << _img << "i)";
	return format.str();
	
//	return "(" + _rel + "," + _img + "i)";
	
}

// Operatoren
Complex& Complex::operator=(const Complex& original){
		_rel = original._rel;
		_img = original._img;
		
		return *this;
		// return Complex(*this);		
}
Complex Complex::operator+(const Complex& original) const{
		double rSumme = _rel + original._rel;
		double iSumme = _img + original._img;
		Complex c(rSumme, iSumme);
		return  c;
}
Complex Complex::operator-(const Complex& original) const {
		return Complex(_rel - original._rel, _img - original._img);
}
		

ostream& operator<<(ostream& output, const Complex& complex){
	output << complex.toString();
	return output;
}
