#ifndef OVERFLOWARRAYSTACKEXCEPTION_H
#define OVERFLOWARRAYSTACKEXCEPTION_H
#include "ArrayStackException.h"

class OutOfRangeArrayStackException : public ArrayStackException
{
public:
	OutOfRangeArrayStackException(const std::string& msg) : ArrayStackException(msg){};
	virtual ~OutOfRangeArrayStackException(){};

};

#endif // OVERFLOWARRAYSTACKEXCEPTION_H
