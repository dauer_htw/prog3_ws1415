#ifndef ARRAYSTACK_H
#define ARRAYSTACK_H
#include <stdexcept>
#include <string>
#include "Stack.h"
#include "InvalidArgumentArrayStackException.h"
#include "OutOfRangeArrayStackException.h"

 using namespace std;


template <class T>
class ArrayStack : public Stack<T> 
{
	
	private:
		int _maxSize;
		int _index;
		T* _last;
		T* _mem;
		void defineLast();
		void checkSize() const;
		void checkOverflow() const;
		void checkUnderflow() const;
	public:
		ArrayStack(int size);
		virtual ~ArrayStack() { delete[] _mem; }
		virtual void push(const T& element);
		virtual T& pop();
		virtual T& top();
		int getSize();

};

template <class T>
ArrayStack<T>::ArrayStack(int maxSize) : _maxSize(maxSize), _index(0), _last(nullptr){
	this->checkSize();
	_mem = new T[_maxSize];
}

template <class T>
void ArrayStack<T>::push(const T& element){
	this->checkOverflow();
	_mem[_index] = element;
	_last = &_mem[_index];
	_index++;
}

template <class T>
void ArrayStack<T>::defineLast(){
	_last = _index > 0 ? &_mem[_index-1] : &_mem[0];
}

template <class T>
T& ArrayStack<T>::pop(){
	this->checkUnderflow();
	T& tmp = *_last;
	_index--;
	this->defineLast();

	return tmp;
}

template <class T>
T& ArrayStack<T>::top(){
	this->checkUnderflow();
	return *_last;
}

template <class T>
int ArrayStack<T>::getSize(){
	return _index;
};


template <class T>
void ArrayStack<T>::checkSize() const{
	if(_maxSize < 2){ 
		string msg("Size was: " + to_string(_maxSize) + " needs to be greater than 1");
		throw InvalidArgumentArrayStackException(msg); 
	}
}
template <class T>
void ArrayStack<T>::checkOverflow() const{
	if(_index >= _maxSize ){ 
		string msg = "stack is full!";
		throw OutOfRangeArrayStackException(msg);
	}
	
} 

template <class T>
void ArrayStack<T>::checkUnderflow() const{
	if(_index == 0){
		string msg = "stack is empty!";
		throw OutOfRangeArrayStackException(msg); 
	}
} 
		
#endif // ARRAYSTACK_H

