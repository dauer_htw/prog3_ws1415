#include "ArrayStackException.h"

ArrayStackException::ArrayStackException(const std::string& msg) : runtime_error(createErrMsg(msg)){};
ArrayStackException::~ArrayStackException(){};


std::string ArrayStackException::createErrMsg(const std::string& msg){
	return "[ArrayStackException] : " + msg;
}