#include "ArrayStackTest.h"
#include "ArrayStack.h"
#include "InvalidArgumentArrayStackException.h"
#include "OutOfRangeArrayStackException.h"

CPPUNIT_TEST_SUITE_REGISTRATION(ArrayStackTest);

void ArrayStackTest::setUp(){
	_intStack = new ArrayStack<int>(23);
}
void ArrayStackTest::tearDown(){
	delete _intStack;
}


void ArrayStackTest::testThatANewStackIsEmpty(){
	CPPUNIT_ASSERT_EQUAL (0, _intStack->getSize());
}

void ArrayStackTest::testThatPushIncreasesTheStackSize(){
	_intStack->push(15);
	CPPUNIT_ASSERT_EQUAL (1,_intStack->getSize());
}

void ArrayStackTest::testThatPushAddsAnElementTotTheTopOfTheStack(){
	_intStack->push(12);
	CPPUNIT_ASSERT_EQUAL (12,_intStack->top());
}

void ArrayStackTest::testThatPopReturnsTheLastElement(){
	_intStack->push(12);
	_intStack->push(13);
	CPPUNIT_ASSERT_EQUAL (13, _intStack->pop());
}

void ArrayStackTest::testThatPopDecreasesTheStackSize(){
	_intStack->push(10);
	_intStack->push(3);
	_intStack->pop();
	CPPUNIT_ASSERT_EQUAL (1, _intStack->getSize());
} 


void ArrayStackTest::testThatTopReturnsTheLastElement(){
	_intStack->push(23);
	_intStack->push(42);
	CPPUNIT_ASSERT_EQUAL (42, _intStack->top());
}

void ArrayStackTest::testThatPushThrowsOutOfRangeException(){
	ArrayStack<double> doubleStack(2);
	doubleStack.push(1.0);
	doubleStack.push(2.0);
	CPPUNIT_ASSERT_THROW(doubleStack.push(3.0),OutOfRangeArrayStackException);
}
void ArrayStackTest::testThatPopThrowsOutOfRangeException(){
	CPPUNIT_ASSERT_THROW(_intStack->pop(),OutOfRangeArrayStackException);
}

void ArrayStackTest::testThatTopThrowsOutOfRangeException(){
	CPPUNIT_ASSERT_THROW(_intStack->top(),OutOfRangeArrayStackException);
}
void ArrayStackTest::testThatANewStackThrowsAInvalidArgumentException(){
	CPPUNIT_ASSERT_THROW(ArrayStack<char> stackToSmall(1),
						InvalidArgumentArrayStackException);
}
		