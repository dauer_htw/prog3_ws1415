#ifndef ARRAYSTACKTEST_H
#define ARRAYSTACKTEST_H
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>
#include "ArrayStack.h"

class ArrayStackTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(ArrayStackTest);
	CPPUNIT_TEST(testThatANewStackIsEmpty);
	CPPUNIT_TEST(testThatPushIncreasesTheStackSize);
	CPPUNIT_TEST(testThatPushAddsAnElementTotTheTopOfTheStack);
	CPPUNIT_TEST(testThatPopReturnsTheLastElement);
	CPPUNIT_TEST(testThatPopDecreasesTheStackSize);
	CPPUNIT_TEST(testThatTopReturnsTheLastElement);
	CPPUNIT_TEST(testThatPushThrowsOutOfRangeException);
	CPPUNIT_TEST(testThatPopThrowsOutOfRangeException);
	CPPUNIT_TEST(testThatTopThrowsOutOfRangeException);
	CPPUNIT_TEST(testThatANewStackThrowsAInvalidArgumentException);
	CPPUNIT_TEST_SUITE_END();
	

	public:
		void setUp();
		void tearDown();
		
		
	protected:
		void testThatANewStackIsEmpty();
		void testThatPushIncreasesTheStackSize();
		void testThatPushAddsAnElementTotTheTopOfTheStack();
		void testThatPopReturnsTheLastElement();
		void testThatPopDecreasesTheStackSize();
		void testThatTopReturnsTheLastElement();
		void testThatPushThrowsOutOfRangeException();
		void testThatPopThrowsOutOfRangeException();
		void testThatTopThrowsOutOfRangeException();
		void testThatANewStackThrowsAInvalidArgumentException();
		
	private:
		ArrayStack<int>* _intStack = nullptr;

};

#endif // ARRAYSTACKTEST_H
