#ifndef COMPLEX_H
#define COMPLEX_H

#include <iostream>

class Complex
{
	private:
		double _rel;
		double _img;
		
	public:
		Complex();
		Complex(double rel);
		Complex(double rel, double img);
		virtual ~Complex(){};
		virtual std::string toString() const;
		
		// Operatoren
		Complex& operator=(const Complex& original);
		friend bool operator==(const Complex& lhs, const Complex& rhs);
		Complex operator+(const Complex& original) const;
		Complex operator-(const Complex& original) const;
		
		// Alternative << Deklaration innerhalb der Klasse mit friend
		//friend std::ostream& operator<<(std::ostream& output, const Complex& complex);
	
};
 
std::ostream& operator<<(std::ostream& output, const Complex& complex);

#endif // COMPLEX_H
