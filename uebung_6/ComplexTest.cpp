#include <string>
#include "ComplexTest.h"

CPPUNIT_TEST_SUITE_REGISTRATION(ComplexTest);

void ComplexTest::setUp(){
	_testComplex_a = new Complex(1,2);
	_testComplex_b = new Complex(3,4);
	_testComplex_c = new Complex(5,6);
}


void  ComplexTest::tearDown(){
	delete _testComplex_a;
	delete _testComplex_b;
	delete _testComplex_c;
}	

void ComplexTest::testAddOperator(){
	CPPUNIT_ASSERT_EQUAL(Complex(4,6), *_testComplex_a + *_testComplex_b);
}

void ComplexTest::testMinusOperator(){
	Complex addResult = *_testComplex_a + *_testComplex_b;
	CPPUNIT_ASSERT_EQUAL(*_testComplex_a, addResult - *_testComplex_b);
}
void ComplexTest::testEqualsOperator(){
	CPPUNIT_ASSERT_EQUAL(true, Complex(5,6) == *_testComplex_c);
	CPPUNIT_ASSERT_EQUAL(false, Complex(6,5) == *_testComplex_c);
}

void ComplexTest::testAssignOperator(){
	Complex newComplex1;
	Complex newComplex2;
	Complex expected(23,5);
	newComplex1 = newComplex2 = expected;
	CPPUNIT_ASSERT_EQUAL(expected, newComplex1);
	CPPUNIT_ASSERT_EQUAL(expected, newComplex2);
}

void ComplexTest::testToString(){
	CPPUNIT_ASSERT_EQUAL(std::string("(3,4i)"), _testComplex_b->toString());
}