#ifndef COMPLEXTEST_H
#define COMPLEXTEST_H
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>
#include "Complex.h"

class ComplexTest : public CppUnit::TestFixture
{

		
	CPPUNIT_TEST_SUITE(ComplexTest);
	CPPUNIT_TEST(testAddOperator);
	CPPUNIT_TEST(testMinusOperator);
	CPPUNIT_TEST(testEqualsOperator);
	CPPUNIT_TEST(testAssignOperator);
	CPPUNIT_TEST(testToString);
	CPPUNIT_TEST_SUITE_END();

	private:
		Complex* _testComplex_a = nullptr;
		Complex* _testComplex_b = nullptr;
		Complex* _testComplex_c = nullptr;
	
	protected:
		void testAddOperator();
		void testMinusOperator();
		void testEqualsOperator();
		void testAssignOperator();
		void testToString();
		
	public:
		void setUp();
		void tearDown();

};

#endif // COMPLEXTEST_H
