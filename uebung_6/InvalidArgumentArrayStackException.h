#ifndef INVALIDARGUMENTARRAYSTACKEXCEPTION_H
#define INVALIDARGUMENTARRAYSTACKEXCEPTION_H
#include "ArrayStackException.h"

class InvalidArgumentArrayStackException : public ArrayStackException
{
	public:
		InvalidArgumentArrayStackException(const std::string& msg) : ArrayStackException(msg) {};
		virtual ~InvalidArgumentArrayStackException(){};
};

#endif // INVALIDARGUMENTARRAYSTACKEXCEPTION_H
