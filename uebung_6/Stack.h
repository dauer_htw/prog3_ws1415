#ifndef STACK_H
#define STACK_H

template <class T>
class Stack
{
	public:
		virtual ~Stack(){};
		virtual void push(const T& element) = 0;
		virtual T& pop() = 0;
		virtual T& top() = 0;
};

#endif // STACK_H
