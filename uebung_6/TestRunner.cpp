#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include "TestRunner.h"
#include "ArrayStackTest.h"


int TestRunner::runTests(){
	CppUnit::TextUi::TestRunner runner;
	CppUnit::TestFactoryRegistry& registry = CppUnit::TestFactoryRegistry::getRegistry();
	runner.addTest(registry.makeTest());
	bool wasSuccessfull = runner.run("", false);
	return !wasSuccessfull;
	
}
